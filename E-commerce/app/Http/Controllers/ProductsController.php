<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products =  Product::orderBy('id','desc')->get();
        return view('backend/products/index', compact('products'));
    }
    public function create()
    {

        return view('backend/products/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:products,title',
                    'description' => 'required|min:20|max:200',
                    'price' => 'required|numeric',

                ]
            );
            Product::create($request->all());
            return redirect()->route('dashboard.products');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
}
