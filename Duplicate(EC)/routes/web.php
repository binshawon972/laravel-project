<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ColorsController;
use App\Http\Controllers\SizesController;
use App\Http\Controllers\BrandsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[FrontendController::class, 'index'])->name('main');





Route::get('/dashboard', function () {
    return view('backend/index');
})->name('dashboard');



Route::get('/dashboard/products',[ProductsController::class, 'index'])->name('dashboard.products');
Route::get('/dashboard/products/create',[ProductsController::class, 'create'])->name('dashboard.products.create');
Route::post('/dashboard/products/store',[ProductsController::class, 'store'])->name('dashboard.products.store');


Route::get('/dashboard/categories',[ CategoriesController::class, 'index'])->name('dashboard.categories');
Route::get('/dashboard/categories/create',[ CategoriesController::class, 'create'])->name('dashboard.categories.create');
Route::post('/dashboard/categories/store',[ CategoriesController::class, 'store'])->name('dashboard.categories.store');

Route::get('/dashboard/colors',[ ColorsController::class, 'index'])->name('dashboard.colors');
Route::get('/dashboard/colors/create',[ ColorsController::class, 'create'])->name('dashboard.colors.create');
Route::post('/dashboard/colors/store',[ ColorsController::class, 'store'])->name('dashboard.colors.store');

Route::get('/dashboard/sizes',[ SizesController::class, 'index'])->name('dashboard.sizes');
Route::get('/dashboard/sizes/create',[ SizesController::class, 'create'])->name('dashboard.sizes.create');
Route::post('/dashboard/sizes/store',[ SizesController::class, 'store'])->name('dashboard.sizes.store');

Route::get('/dashboard/brands',[ BrandsController::class, 'index'])->name('dashboard.brands');
Route::get('/dashboard/brands/create',[ BrandsController::class, 'create'])->name('dashboard.brands.create');
Route::post('/dashboard/brands/store',[ BrandsController::class, 'store'])->name('dashboard.brands.store');
