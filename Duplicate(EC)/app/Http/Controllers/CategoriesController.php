<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories =  Category::orderBy('id','desc')->get();
        return view('backend/categories/index', compact('categories'));
    }
    public function create()
    {
        return view('backend/categories/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:categories,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Category::create($request->all());
            return redirect()->route('dashboard.categories');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
}
