<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public function index()
    {
        $brands =  Brand::orderBy('id','desc')->get();
        return view('backend/brands/index', compact('brands'));
    }
    public function create()
    {
        return view('backend/brands/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:brands,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Brand::create($request->all());
            return redirect()->route('dashboard.brands');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
}
