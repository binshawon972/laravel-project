<?php
use App\Http\Controllers\TestController;
use App\Http\Controllers\Test1Controller;
use App\Http\Controllers\Test2Controller;
use App\Http\Controllers\Test3Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/test', function () {
//     echo "This is laravel Test.";
//     //return view('welcome');
// });

// Route::get('/product', function () {
//     echo "This is laravel Product.";
//     //return view('welcome');
// });

// Route::get('/category', function () {
//     echo "This is laravel Category.";
//     //return view('welcome');
// });

// Route::get('/class', function () {
//     echo "This is laravel Class.";
//     //return view('welcome');
// });

// Route::get('/user', function () {
//     echo "This is laravel User.";
//     //return view('welcome');
// });

Route::get('/test',[TestController::class, 'index']);
Route::get('/test1',[Test1Controller::class, 'index']);
Route::get('/test2',[Test2Controller::class, 'index']);
Route::get('/test3',[Test3Controller::class, 'index']);
